﻿# RemoveQueryBuilderFields

## Component Information
* Author: Jonathan Hult
* Last Updated: build_1_20120621
* License: MIT

## Overview
This component overrides the dynamichtml include "query_form_std_script" in order to remove metadata fields from the Query Builder search form. The metadata fields to remove are specified in a preference prompt.
	
* Preference Prompts:
	RemoveQueryBuilderFields_FieldList - A comma-separated list of metadata field names to remove from Query Builder form.

## Compatibility
This component was built upon and tested on the version listed below, this is the only version it is known to work on, but it may work on older/newer versions.

* 11gR1-11.1.1.6.0-idcprod1-111219T111403 (Build: 7.3.3.183)

## Changelog
* build_1_20120621
	- Initial component release